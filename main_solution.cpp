////////////////////////////////////////////////////////////////////////
// OOP Tutorial 9: Class Derivation (static binding) (version 0)
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <iostream>	//for cin >> and cout <<
#include <iomanip>	//for cin >> and cout <<
#include <cassert>	//for assert
#include <conio.h>	//for _kbhit
#include <string>	//for string

#include "BankAccount.h"
#include "CurrentAccount.h"
#include "SavingsAccount.h"

using namespace std;

int main()
{
	double getPositiveValue();
/*
	double amount;

	cout << "\n\nCREATE bAcct1... \n";
	BankAccount bAcct1( "001", "12-00", 100.0);

	cout << "\n\nPRINT MINI STATEMENT FOR bAcct1... \n" << bAcct1.prepareStatement() << endl;

	cout << "\n\nSHOW BALANCE " << (char)156 << bAcct1.getBalance();

	cout << "\n\nPRINT MINI STATEMENT FOR bAcct1... \n" << bAcct1.prepareStatement() << endl;

	cout << "\n\nENTER AMOUNT TO DEPOSIT: " << (char)156;
	amount = getPositiveValue();
	bAcct1.deposit( amount);

	cout << "\n\nPRINT MINI STATEMENT FOR bAcct1... \n" << bAcct1.prepareStatement() << endl;

	cout << "\n\nCREATE bAcct2... \n";
	BankAccount bAcct2( "002", "12-00", 0.0);
	cout << "\n\nPRINT MINI STATEMENT FOR bAcct2... \n" << bAcct2.prepareStatement() << endl;


	//Question 2a
	BankAccount bankAcct1;
	cout << bankAcct1.getBalance();
	cout << bankAcct1.prepareStatement();

	//Question 2b
	BankAccount bankAcct2( "001", "00-44", 500.0);
	bankAcct2.deposit( 200.0);
	cout << bankAcct2.prepareStatement();

	//Question 2c
	BankAccount bankAcct3( "002", "00-44", 500.0);
//	cout << bankAcct3.getOverdraftLimit(); //error C2039: 'getOverdraftLimit' : is not a member of 'BankAccount'


	//Question 3a
	CurrentAccount currAcct1( "002", "00-44", 500.0, 25.0);
	cout << currAcct1.getBalance();

    //Question 3b
	CurrentAccount currAcct2;
	cout << currAcct2.prepareStatement()
		 << "\nOVERDRAFT LIMIT: " << (char)156 
		 << currAcct2.getOverdraftLimit();
		
	//Question 3c
	CurrentAccount currAcct3("001", "00-44", 500.0, 25.0);
	currAcct3.deposit( 200.0);
	cout << currAcct3.prepareStatement()
		 << "\nOVERDRAFT LIMIT: " << (char)156 
		 << currAcct3.getOverdraftLimit();
		
	//Question 3d
	CurrentAccount currAcct4("001", "00-44", 500.0, 25.0);
	CurrentAccount currAcct5( currAcct4);
	currAcct4.deposit( 200.0);
	cout << currAcct4.prepareStatement()
		 << "\nOVERDRAFT LIMIT: " << (char)156 
		 << currAcct4.getOverdraftLimit();
	cout << currAcct5.prepareStatement()
		 << "\nOVERDRAFT LIMIT: " << (char)156 
		 << currAcct5.getOverdraftLimit();

	//Question 3e
	CurrentAccount currAcct6( "002", "00-44", 500.0, 25.0);
//	cout << currAcct6.balance_; //error C2248: 'balance_' : cannot access private member declared in class 'BankAccount'

	//Question 3f
	CurrentAccount currAcct7( "002", "00-44", 500.0, 25.0);
	cout << currAcct7.getOverdraftLimit();

  //Question 3g
	CurrentAccount currAcct8( "002", "00-44", 500.0, 25.0);
//	cout << currAcct8.overdraftLimit_; //error C2248: 'overdraftLimit_' : cannot access private member declared in class 'CurrentAccount'
 
	//Question 4a
	double amount;
	BankAccount bankAcct4;
	cout << "\nENTER AMOUNT TO CREDIT: " << (char)156;
	amount = getPositiveValue();
	bankAcct4.deposit( amount);
	cout << "\nENTER AMOUNT TO WITHDRAW: " << (char)156;
	amount = getPositiveValue();
	bankAcct4.withdraw( amount);
	cout << bankAcct4.prepareStatement();

	//Question 4b
	BankAccount bankAcct5( "001", "12-00", 100.0);
	BankAccount bankAcct6( "001", "12-00", 120.0);
	BankAccount bankAcct7( "032", "12-00", 120.0);
	if (bankAcct5 == bankAcct6) 
		cout << "\nSAME ACCOUNTS!";
	else
		cout << "\nDIFFERENT ACCOUNTS!";
	if (bankAcct5 != bankAcct6) 
		cout << "\nDIFFERENT ACCOUNTS!";
	else
		cout << "\nSAME ACCOUNTS!";

	//Question 4c
	double amount;
	BankAccount bankAcct7;
	cout << "\nENTER AMOUNT TO CREDIT: " << (char)156;
	amount = getPositiveValue();
	bankAcct7.deposit( amount);
	cout << "\nENTER AMOUNT TO WITHDRAW: " << (char)156;
	amount = getPositiveValue();
	bankAcct7.withdraw( amount);
	cout << bankAcct7.prepareStatement();

	//Question 5a
	CurrentAccount currAcct9("001", "00-44", 500.0, 25.0);
	cout << "\nOVERDRAFT LIMIT: " << (char)156 
		 << currAcct9.getOverdraftLimit();
	currAcct9.setOverdraftLimit( 50);
	cout << "\nOVERDRAFT LIMIT: " << (char)156 
		 << currAcct9.getOverdraftLimit();

	//Question 5b
	CurrentAccount currAcct10a("001", "00-44", 500.0, 40.0);
	cout << "\n\nBefore withdrawal..." << currAcct10a.prepareStatement();
	currAcct10a.withdraw( 520.0);
	cout << "\nAfter withdrawal..." << currAcct10a.prepareStatement();
	
	CurrentAccount currAcct10b("001", "00-44", 500.0, 10.0);
	cout << "\n\nBefore withdrawal..." << currAcct10b.prepareStatement();
	currAcct10b.withdraw( 520.0);
	cout << "\nAfter withdrawal..." << currAcct10b.prepareStatement();
*/
/*	//Question 5c
	CurrentAccount currAcct11("001", "00-44", 500.0, 25.0);
	cout << currAcct11.prepareStatement();

	//Question 6
	double amount;
	SavingsAccount savAcct1;
	cout << savAcct1.prepareStatement();

	SavingsAccount savAcct2("001", "00-44", 500.0, 10.0, 100.0);
	cout << savAcct2.prepareStatement();
	cout << "\nENTER AMOUNT TO CREDIT: " << (char)156;
	amount = getPositiveValue();
	savAcct2.deposit( amount);
	cout << savAcct2.prepareStatement();
	cout << "\nENTER AMOUNT TO CREDIT: " << (char)156;
	amount = getPositiveValue();
	savAcct2.deposit( amount);
	cout << savAcct2.prepareStatement();


	//Question 7
	void printMiniStatement( BankAccount bA);
	BankAccount bA; 
	CurrentAccount cA; 

	bA = cA; 
	BankAccount bA2( cA); 
	printMiniStatement( cA);

	//Question 7a

	CurrentAccount currAcct( "002", "00-44", 500.0, 25.0);
	BankAccount bankAcct( currAcct);
	cout << bankAcct.getBalance();
	cout << bankAcct.prepareStatement();
//	cout << bankAcct.getOverdraftLimit();  //error C2039: 'getOverdraftLimit' : is not a member of 'BankAccount'

	//Question 7b
	BankAccount bankAcct;
	CurrentAccount currAcct( bankAcct);  //error C2664: '__thiscall CurrentAccount::CurrentAccount(const class CurrentAccount &)' : cannot convert parameter 1 from 'class BankAccount' to 'const class CurrentAccount &'
										 //Reason: cannot convert from 'class BankAccount' to 'const class CurrentAccount'

	//Question 7c

	void printMiniStatement( BankAccount bA);
	BankAccount bankAcct( "101", "00-44", 500.0);
	printMiniStatement( bankAcct);
	CurrentAccount currAcct( "201", "00-44", 500.0, 25.0);
	printMiniStatement( currAcct);
*/
	//Question 9

	BankAccount ba("101", "00-44", 500.0);
	for (int i(0); i < 5; ++i)
		ba.deposit(10.0*i);
	cout << "\nba transaction count: " << ba.getTransactionCount();
	ba.resetHistory(3);
	cout << "\nba transaction count: " << ba.getTransactionCount();

	for (int i(0); i < 5; ++i)
		ba.deposit(10.0*i);
	cout << "\nba transaction count: " << ba.getTransactionCount();
	ba.resetHistory();
	cout << "\nba transaction count: " << ba.getTransactionCount();

	CurrentAccount ca("201", "00-44", 500.0, 25.0);
	for (int i(0); i < 5; ++i)
		ca.deposit(10.0*i);
	cout << "\nca transaction count: " << ca.getTransactionCount();
	ca.resetHistory();
	cout << "\nca transaction count: " << ca.getTransactionCount();

	for (int i(0); i < 5; ++i)
		ca.deposit(10.0*i);
	cout << "\nca transaction count: " << ca.getTransactionCount();
	ca.resetHistory(3); //error too many arguments in fuuntion call
	cout << "\nca transaction count: " << ca.getTransactionCount();



/*
*/	while (!_kbhit());
	return 0;
}

double getPositiveValue() {
	double amount;
	cin >> amount;
	while ( amount < 0)
	{
		cout << "\nERROR: AMOUNT MUST BE POSITIVE: " << (char)156;
		cin >> amount;
	}
	return amount;
}

void printMiniStatement( BankAccount bA) {
	Date today;
	cout << "\n-----------------------------------------";
	cout << "\nMINI STATEMENT\t" << today;
	cout << "\n-----------------------------------------";
	cout << bA.prepareStatement();
	cout << "\n-----------------------------------------";
}
