
#ifndef SavingsAccountH
#define SavingsAccountH

//SavingsAccount: class declaration

#include "BankAccount.h"

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cassert>
using namespace std;

class SavingsAccount : public BankAccount {
public:
	SavingsAccount();
	SavingsAccount( string aN, string sC, double b, double iR, double mD);
	double getInterestRate() const;
	double getMinimumDeposit() const ;
	bool canDeposit( double a) const;
	void deposit( double a);
	string prepareStatement() const;
private:
	double interestRate_;
	double minimumDeposit_;
};

#endif
