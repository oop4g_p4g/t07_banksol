
#include "SavingsAccount.h"

//SavingsAccount: class implementation

//---------------------------------------------------------------------------
//public member functions

//constructors
SavingsAccount::SavingsAccount()
: interestRate_( 0.0),
  minimumDeposit_( 100.0)
{}
SavingsAccount::SavingsAccount( string aN, string sC,
                                double b, double iR, double mD)
: BankAccount( aN, sC, b),
  interestRate_( iR),
  minimumDeposit_( mD)
{}

//other public member functions
double SavingsAccount::getMinimumDeposit() const {
	return minimumDeposit_;
}

double SavingsAccount::getInterestRate() const {
	return interestRate_;
}

string SavingsAccount::prepareStatement() const {
	ostringstream os;
	//display inherited BankAccount attributes
	os << BankAccount::prepareStatement();
	os << fixed << setprecision(2);
	//display interest rate
	os << "\nINTEREST RATE:            " << interestRate_ << "%";
	//display minimum deposit
	os << "\nMINIMUM DEPOSIT:          " << (char)156 << minimumDeposit_;
	return ( os.str());
}

bool SavingsAccount::canDeposit( double a) const {
	return ( minimumDeposit_ <= a);
}
void SavingsAccount::deposit( double amountToDeposit) {
	if ( canDeposit( amountToDeposit))
	{
		recordTransaction( amountToDeposit); 
		cout << "\nOK: DEPOSIT AUTHORISED! " << (char)156 
		     << amountToDeposit<< "!";
	}
	else
		cout << "\nERROR: INSUFFICIANT FUNDS TO DEPOSIT " << (char)156 
		     << amountToDeposit<< "!";
}

//---------------------------------------------------------------------------
//private member functions
