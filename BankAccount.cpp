
#include "BankAccount.h"

//BankAccount: class implementation

//---------------------------------------------------------------------------
//public member functions

//constructors
BankAccount::BankAccount()
    : accountNumber_( ""), sortCode_( ""),
      creationDate_( Date( 0, 0, 0)),
      balance_( 0.0),
      transactionCount_(0)
{}
BankAccount::BankAccount( string aN, string sC, double b)
    : accountNumber_( aN), sortCode_( sC),
      creationDate_( Date()),
      balance_( b),
      transactionCount_(0)
{}
BankAccount::~BankAccount()
{}

//other public member functions
string BankAccount::getAccountNumber() const {
    return accountNumber_;
}
string BankAccount::getSortCode() const {
    return sortCode_;
}
Date BankAccount::getCreationDate() const {
    return creationDate_;
}
double BankAccount::getBalance() const {
    return balance_;
}
int BankAccount::getTransactionCount() const {
	return transactionCount_;
}
bool BankAccount::canWithdraw( double a) const {
	return ( balance_ >= a);
}
void BankAccount::recordTransaction( double amount) {
	++transactionCount_;	//update number of recorded transactions
	updateBalance( amount);	//update balance
}
void BankAccount::withdraw( double amountToWithdraw) {
	assert ( amountToWithdraw >= 0);
	if ( canWithdraw( amountToWithdraw))
	{
		recordTransaction( -amountToWithdraw);
		cout << "\nOK: WITHDRAWAL AUTHORISED!" << (char)156 
		     << amountToWithdraw << "!";
	}
	else
		cout << "\nERROR: INSUFFICIANT FUNDS TO WITHDRAW! " << (char)156 
		     << amountToWithdraw << "!";
}
void BankAccount::deposit( double amountToDeposit) {
	assert ( amountToDeposit >= 0);
	recordTransaction( amountToDeposit);
	cout << "\nOK: DEPOSIT AUTHORISED! " << (char)156 
		     << amountToDeposit<< "!";
}
string BankAccount::prepareStatement() const {
	ostringstream os;
	//display account number
	os << "\nACCOUNT NUMBER:           " << accountNumber_;
	//display sort code
	os << "\nSORT CODE:                " << sortCode_;
	//display creation date
	os << "\nCREATION DATE:            " << creationDate_;
	//display balance_
	os << fixed << setprecision(2);
	os << "\nBALANCE:                  " << (char)156 << balance_;
	//display transactionCount_
	os << "\nNUMBER OF TRANSACTIONS:   " << transactionCount_;
	return ( os.str());
}
bool BankAccount::operator ==( const BankAccount& bA) const {
	return ( ( accountNumber_ == bA.accountNumber_) && 
	         ( sortCode_ == bA.sortCode_));
}
bool BankAccount::operator !=( const BankAccount& bA) const {
	return ( ! ((*this) == bA));
}
void BankAccount::resetHistory() {
	cout << "\nClear BA transaction history!";
	transactionCount_ = 0;
}
void BankAccount::resetHistory(int x) {
	cout << "\nClean BA transaction history!";
	transactionCount_ = (transactionCount_ < x) ? transactionCount_ : x;
}
//---------------------------------------------------------------------------
//private member functions
void BankAccount::updateBalance( double amount) {
    balance_ = balance_ + amount;   //add/take amount to/from balance_
}


/////////////////////
