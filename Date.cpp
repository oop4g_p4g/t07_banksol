#include <assert.h>

#include "Date.h"

//---------------------------------------------------------------------------
//Date: class implementation
//---------------------------------------------------------------------------

//____public member functions

//____constructors & destructors
Date::Date() 		//default constructor
{
	setToCurrentDate();
}
Date::Date( int d, int m, int y) 	//constructor
	: day_( d), month_( m), year_( y)
{}

//____other public member functions
int Date::getDay() const {
	return day_;
}
int Date::getMonth() const {
	return month_;
}
int Date::getYear() const {
	return year_;
}
void Date::setDate( int d, int m, int y) {
	day_ = d;
	month_ = m;
	year_ = y;
}
void Date::setToCurrentDate() {  	// sets to system date
	time_t now = time(0);
	struct tm t;
	if (localtime_s(&t, &now) != 0)
		assert(false);
	day_ = t.tm_mday;
	month_ = t.tm_mon + 1;
	year_ = t.tm_year + 1900;
}

//____operator overloading functions 
bool Date::operator==( const Date& d) const { //comparison operator
	return
		(( day_ == d.day_) &&
		 ( month_ == d.month_) &&
		 ( year_ == d.year_));
}

ostream& Date::putDataInStream( ostream& os) const {
						//date formatted output (DD/MM/YY)
	os << setfill('0');
	os << setw(2) << day_ << "/";
	os << setw(2) << month_ << "/";
	os << setw(4) << year_;
	return os;
}
istream& Date::getDataFromStream( istream& is) {
						//read in date as DD/MM/YY
	char ch;			//(any) colon field delimiter
	is >> day_ >> ch >> month_ >> ch >> year_;
	return is;
}


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<( ostream& os, const Date& aDate) {
    return ( aDate.putDataInStream( os));
}
istream& operator>>( istream& is, Date& aDate) {
	return ( aDate.getDataFromStream( is));
}
