
#ifndef CurrentAccountH
#define CurrentAccountH

//CurrentAccount: class declaration


#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;

#include "BankAccount.h"

class CurrentAccount : public BankAccount {
public:
	CurrentAccount();
	CurrentAccount( string aN, string sC, double b, double oL);
	CurrentAccount( const CurrentAccount&);
	~CurrentAccount();
	CurrentAccount& operator=( const CurrentAccount& c);
	double getOverdraftLimit() const;
	void setOverdraftLimit( double oL);
	bool canWithdraw( double a) const;
	void withdraw( double amountToWithdraw);
	string prepareStatement() const;
	using BankAccount::resetHistory;
	void resetHistory();
private:
	double overdraftLimit_;
};


#endif
