
#include "CurrentAccount.h"

//CurrentAccount: class implementation

//---------------------------------------------------------------------------
//public member functions

//constructors
CurrentAccount::CurrentAccount()
: overdraftLimit_( 0.0)
{}
CurrentAccount::CurrentAccount( string aN, string sC,
                                double b, double oL)
: BankAccount( aN, sC, b),
  overdraftLimit_( oL)
{}

CurrentAccount::CurrentAccount( const CurrentAccount& c)
: BankAccount( c),
  overdraftLimit_( c.overdraftLimit_)
{}
CurrentAccount::~CurrentAccount()
{}

CurrentAccount& CurrentAccount::operator=( const CurrentAccount& c) {
	if (&c != this) 	//check for self-assignment
	{
		this->BankAccount::operator=( c);
		overdraftLimit_ = c.overdraftLimit_;
	}
	return *this;
}

//other public member functions
double CurrentAccount::getOverdraftLimit() const {
    return overdraftLimit_;
}
void CurrentAccount::setOverdraftLimit( double oL) {
    overdraftLimit_ = oL;
}
bool CurrentAccount::canWithdraw( double a) const {
	return ( (getBalance() + overdraftLimit_) >= a);
}
void CurrentAccount::withdraw( double amountToWithdraw) {
	if ( canWithdraw( amountToWithdraw))
	{
		recordTransaction( -amountToWithdraw);
		cout << "\nOK: WITHDRAWAL AUTHORISED!" << (char)156 
		     << amountToWithdraw << "!";
	}
	else
	{
		cout << "\nERROR: INSUFFICIANT FUNDS TO WITHDRAW! " << (char)156 
		     << amountToWithdraw << "!";
		cout << "\nPLEASE CONTACT YOUR BANK TO REARRANGE OVERDRAFT!";
	}
}
string CurrentAccount::prepareStatement() const {
	ostringstream os;
	//display inherited BankAccount attributes
	os << BankAccount::prepareStatement();
	os << fixed << setprecision(2);
	//display overdraft limit
	os << "\nOVERDRAFT LIMIT:          " << (char)156 << overdraftLimit_;
	return ( os.str());
}
void CurrentAccount::resetHistory() {
	cout << "\nClear CA transaction history!";
	BankAccount::resetHistory();
}
//---------------------------------------------------------------------------
//private member functions
