#include <iostream>
#include <conio.h>	//for _kbhit
using namespace std;

class Base
{
public:

   void f(char)
   {
      cout << "Base::f(char);\n";
   }
   void g() const
   {
      cout << "Base::g() const;\n";
   }

};

class Derived : public Base
{
public:
   using Base::f;
   using Base::g;

   void f(int)
   {
      cout << "Derived::f(int);\n";
   }
   void g() 
   {
      cout << "Derived::g();\n";
   }

};

int main()
{
    Base b;
    Derived d;
    const Base cb;
    const Derived cd;
  
    // Call f using an int as the argument
    b.f(10);
    d.f(10);
    
    // Call f using a char as the argument
    b.f('a');
    d.f('a');

     // Call g 
    b.g();
    cb.g();
    d.g();
	cd.g(); 	//*** - error C2662: 'Derived::g' : cannot convert 'this' pointer from 'const Derived' to 'Derived &'

	while (!_kbhit());
	return 0;
}
