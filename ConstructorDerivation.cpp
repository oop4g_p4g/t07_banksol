#include <iostream>
#include <conio.h>	//for _kbhit
using namespace std;

class Base
{
public:
	Base(): b_(0) {
		cout << "\nConstructor Base() ";
	};
	Base(int x): b_(x) {
		cout << "\nConstructor Base(int) "<< x;
	};

	void showBase() const {
		cout << "\nBase::b_" << b_;
	}
private:
	int b_;
};

class Derived : public Base
{
public:

	Derived(): d_(0){
		cout << "\nConstructor Derived()";
	};
	Derived(int x): Base(x), d_( 2*x){
		cout << "\nConstructor Derived()" << x;
	};
	Derived& operator=( const Derived& d) {
		d_ = d.d_;
		return *this;
	}
	void showDerived() const {
		Base::showBase();
		cout << "\nDerived::d_" << d_;
	}
private:
	int d_;
};

int main()
{
	Base b;
	Base bb(5);
	b.showBase();
	bb.showBase();
	Derived d;
	Derived dd(12);
	d.showDerived();
	dd.showDerived();
	cout << "\n\n";
	//using Derived assignment operator
	//	d = dd;
	//using Base assignment operator
	d.Base::operator=(dd);
	d.showDerived();

	while (!_kbhit());
	return 0;
}
