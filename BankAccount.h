
#ifndef BankAccountH
#define BankAccountH

//BankAccount: class declaration

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cassert>
using namespace std;

#include "Date.h"

class BankAccount {
public:
	BankAccount();
	BankAccount( string aN, string sC, double b);
	~BankAccount();
	string getAccountNumber() const;
	string getSortCode() const;
	Date getCreationDate() const;
	int getTransactionCount() const;
	double getBalance() const;
	bool canWithdraw( double a) const;
	void recordTransaction( double amount);
	void withdraw( double);
	void deposit( double);
	string prepareStatement() const;
	bool operator ==( const BankAccount& bA) const;
	bool operator !=( const BankAccount& bA) const;
	void resetHistory();
	void resetHistory(int);
private:
	//data items
	string accountNumber_;
	string sortCode_;
	Date creationDate_;
	double balance_;
	int transactionCount_;
	//support functions
	void updateBalance( double a);
};

#endif
