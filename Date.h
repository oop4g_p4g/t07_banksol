
#ifndef DateH
#define DateH

//---------------------------------------------------------------------------
//Date_: class declaration
//---------------------------------------------------------------------------

#include <time.h>	// for date functions

#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

class Date {
public:
	Date();	//default constructor
	Date( int, int, int);	//constructor
	int getDay() const;		//return a data member value, day_
	int getMonth() const;		//return a data member value, month_
	int getYear() const;		//return a data member value, year_
	void setDate( int, int, int);	//set new values for date
    void setToCurrentDate();    //set date from the system
	bool operator==( const Date&) const;  	//comparison operator
	ostream& putDataInStream( ostream& os) const;	//send Date info into an output stream
	istream& getDataFromStream( istream& is);	//receive Date info from an input stream
private:
	int day_;
	int month_;
	int year_;
};

ostream& operator<<( ostream&, const Date&);	//output operator
istream& operator>>( istream&, Date&);	//input operator

#endif
